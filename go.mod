module gitlab.com/mig4/gokit-experiment

go 1.12

require (
	github.com/VividCortex/gohistogram v1.0.0 // indirect
	github.com/go-kit/kit v0.9.0
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/prometheus/client_golang v1.1.0
)
