package main

import (
	"net/http"
	"os"

	"github.com/go-kit/kit/log"

	"gitlab.com/mig4/gokit-experiment/service"
)

func main() {
	logger := log.NewLogfmtLogger(os.Stderr)
	service.SetupHandlers(logger)
	logger.Log("msg", "HTTP", "addr", ":58080")
	logger.Log("err", http.ListenAndServe(":58080", nil))
}
