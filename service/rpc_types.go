package service

type uppercaseRequest struct {
	Str string `json:"str"`
}

type uppercaseResponse struct {
	Value string `json:"v"`
	Err   string `json:"err,omitempty"`
}

type countRequest struct {
	Str string `json:"str"`
}

type countResponse struct {
	Value int `json:"value"`
}
