package service

import (
	"errors"
	"strings"
)

type StringService interface {
	Uppercase(string) (string, error)
	Count(string) int
}

// Implementation

var ErrEmpty = errors.New("Empty string")

type stringService struct{}

func (stringService) Uppercase(s string) (string, error) {
	if s == "" {
		return s, ErrEmpty
	}
	return strings.ToUpper(s), nil
}

func (stringService) Count(s string) int {
	return len(s)
}
