package service

import (
	"context"

	"github.com/go-kit/kit/endpoint"
)

func makeUppercaseEndpoint(svc StringService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(uppercaseRequest)
		value, err := svc.Uppercase(req.Str)
		if err != nil {
			return uppercaseResponse{value, err.Error()}, nil
		}
		return uppercaseResponse{value, ""}, nil
	}
}

func makeCountEndpoint(svc StringService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(countRequest)
		value := svc.Count(req.Str)
		return countResponse{value}, nil
	}
}
