# GoKit experiment

run: `go run main.go`

and then:

``` sh
curl -X POST -d '{"str": "hello world"}' http://localhost:58080/uppercase
curl -X POST -d '{"str": "hello world"}' http://localhost:58080/count
curl -X POST -d '{"str": "hello world"}' http://localhost:58080/metrics
```
